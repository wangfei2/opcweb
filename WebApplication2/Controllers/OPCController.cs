﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Core;

namespace WebApplication2.Controllers
{
    public class OPCController : Controller
    {
        // GET: OPC
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取所有OPC实时数据
        /// </summary>
        /// <returns></returns>
        public JObject AllData()
        {
            var list = OPCClient.OPCDataItems.Values.ToList();
            var result = new
            {
                code = list.Count == 0 ? 0 : 1,
                data = list
            };
            return JObject.FromObject(result);
        }
    }
}