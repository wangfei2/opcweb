﻿using OPCAutomation;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication2.Core
{
    public class OPCJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                UPdateOPCData();
            });
        }

        private void UPdateOPCData()
        {
            if (OPCClient.OPCServer.ServerState != (int)(OPCServerState.OPCRunning))
            {
                OPCClient.StartOPCClient();
            }

            var serverHandleList = OPCClient.ServerMap.Keys.ToArray();//服务端句柄集合

            if (serverHandleList.Length > 1)
            {
                OPCClient.OPCGroup.SyncRead(1, serverHandleList.Length - 1, serverHandleList, out Array values, out Array errors, out object qualities, out object timestamps);
                for (int i = 1; i < serverHandleList.Length; i++)
                {
                    var serverHandle = serverHandleList[i];
                    var clienHandle = OPCClient.ServerMap[serverHandle];
                    var value = values.GetValue(i).ToString();
                    var quality = Convert.ToInt32(((Array)qualities).GetValue(i));
                    var timestamp = Convert.ToDateTime(((Array)(timestamps)).GetValue(i)).ToLocalTime();

                    OPCClient.OPCDataItems[clienHandle].Value = value;
                    OPCClient.OPCDataItems[clienHandle].UpdateTime = timestamp;
                    OPCClient.OPCDataItems[clienHandle].Quality = quality;
                }
            }

        }
    }
}