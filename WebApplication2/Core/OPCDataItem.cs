﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Core
{
    public class OPCDataItem
    {
        /// <summary>
        /// 客户端句柄
        /// </summary>
        public int ClientHandle { get; set; }

        /// <summary>
        /// OPC项
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// OPC注册项
        /// </summary>
        public OPCItem OPCItem { get; set; }

        /// <summary>
        /// OPC项值
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// OPC项更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// OPC项质量
        /// </summary>
        public int Quality { get; set; }
    }
}