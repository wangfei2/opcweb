﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Core
{
    public class OPCClient
    {
        /// <summary>
        /// OPC服务器
        /// </summary>
        public static OPCServer OPCServer = new OPCServer();

        /// <summary>
        /// OPC组
        /// </summary>
        public static OPCGroup OPCGroup;

        /// <summary>
        /// OPC实时数据
        /// </summary>
        public static Dictionary<int, OPCDataItem> OPCDataItems = new Dictionary<int, OPCDataItem>();
        /// <summary>
        /// OPC句柄映射
        /// </summary>
        public static Dictionary<int, int> ServerMap = new Dictionary<int, int> { { 0, 0 } };

        /// <summary>
        /// 启动客户端
        /// </summary>
        public static void StartOPCClient()
        {
            ServerMap = new Dictionary<int, int> { { 0, 0 } };
            OPCDataItems = new Dictionary<int, OPCDataItem>();

            if (OPCServer.ServerState != (int)(OPCServerState.OPCRunning))
            {
                OPCServer.Connect("KEPware.KEPServerEx.V4");

                var groups = OPCServer.OPCGroups;
                groups.DefaultGroupDeadband = 0;
                groups.DefaultGroupIsActive = true;
                groups.DefaultGroupUpdateRate = 1000;

                OPCGroup = groups.Add("OPC");
                var itemCollection = GetOPCItems();
                foreach (var item in itemCollection)
                {
                    try
                    {

                        var OPCItems = OPCGroup.OPCItems;
                        var opcItem = OPCItems.AddItem(item.Tag, item.ClientHandle);
                        item.OPCItem = opcItem;
                        OPCDataItems.Add(item.ClientHandle, item);
                        ServerMap.Add(item.OPCItem.ServerHandle, item.ClientHandle);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.logerror.Error(ex);
                    }
                }
            }


        }

        /// <summary>
        /// 获取OPC点位数据集合
        /// </summary>
        /// <returns></returns>
        private static List<OPCDataItem> GetOPCItems()
        {
            List<OPCDataItem> dataItems = new List<OPCDataItem>();
            dataItems.Add(new OPCDataItem
            {
                ClientHandle = 1,
                Tag = "Channel_0_User_Defined.Random.Random3",
            });
            dataItems.Add(new OPCDataItem
            {
                ClientHandle = 2,
                Tag = "Channel_0_User_Defined.Random.RandomXaxis",
            });
            return dataItems;
        }
    }
}