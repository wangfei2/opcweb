﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Core
{
    public class OPCScheduler
    {
        public static void RunTask()
        {
            ISchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = factory.GetScheduler().Result;


            IJobDetail job = JobBuilder.Create<OPCJob>()
                .WithIdentity("job_opc", "joc_opcgroup")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger_opc", "trigger_opcgroup")
                //.WithCronSchedule("0/1 * * * * ?")
                .WithSimpleSchedule(x =>
                {
                    x.WithIntervalInSeconds(1);
                    x.RepeatForever();
                })
                .StartNow()
                .Build();

            scheduler.ScheduleJob(job, trigger);
            scheduler.Start();
        }
    }
}