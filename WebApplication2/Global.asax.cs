﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication2.Core;

namespace WebApplication2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //log4net 配置文件路径管理
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\log4net.config"));

            OPCClient.StartOPCClient();
            OPCScheduler.RunTask();

            LogHelper.loginfo.Info("Web Server Started...");
        }

        protected void Application_End()
        {
            if (OPCClient.OPCServer.ServerState == (int)(OPCServerState.OPCRunning))
            {
                OPCClient.OPCServer.Disconnect();
            }

            LogHelper.loginfo.Info("Web Server ShutDown...");
        }
    }
}
